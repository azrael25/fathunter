var app = angular.module('App', []);

var types = [
    ['низкоуглеводный день 1', [2.3, 0.9, 0.8, 20, 40, 40, 40, 30, 30, 70, 30, 0], [3.0, 0.8, 1.0, 20, 40, 40, 40, 30, 30, 70, 30, 0]],
    ['низкоуглеводный день 2', [2.5, 1.0, 0.5, 30, 30, 40, 30, 40, 30, 100, 0, 0], [3.0, 0.8, 0.7, 30, 30, 40, 30, 40, 30, 100, 0, 0]],
    ['низкоуглеводный день 3', [2.5, 1.0, 0.5, 30, 30, 40, 40, 40, 20, 100, 0, 0], [3.0, 0.8, 0.7, 30, 30, 40, 40, 40, 20, 100, 0, 0]],
    ['средеуглеводный день 1', [2.0, 0.8, 1.5, 20, 40, 40, 40, 30, 30, 50, 50, 0], [2.5, 0.7, 2.0, 20, 40, 40, 40, 30, 30, 50, 50, 0]],
    ['средеуглеводный день 2', [2.0, 0.8, 1.3, 30, 30, 40, 40, 30, 30, 50, 50, 0], [2.5, 0.7, 1.7, 30, 30, 40, 40, 30, 30, 50, 50, 0]],
    ['средеуглеводный день 3', [2.0, 0.8, 1.3, 30, 30, 40, 40, 40, 20, 50, 50, 0], [2.5, 0.7, 1.7, 30, 30, 40, 40, 40, 20, 50, 50, 0]],
    ['высокоуглеводный день 1', [1.5, 0.8, 2.5, 30, 30, 40, 40, 30, 30, 60, 40, 0], [2.0, 0.7, 3.0, 30, 30, 40, 40, 30, 30, 60, 40, 0]],
    ['высокоуглеводный день 2', [1.5, 0.8, 2.3, 30, 30, 40, 40, 30, 30, 60, 40, 0], [2.0, 0.7, 2.8, 30, 30, 40, 40, 30, 30, 60, 40, 0]],
    ['высокоуглеводный день 3', [1.5, 0.8, 2.3, 30, 30, 40, 40, 40, 20, 60, 40, 0], [2.0, 0.7, 2.8, 30, 30, 40, 40, 40, 20, 60, 40, 0]]
];

app.controller('AppController', function AppController($scope) {
    $scope.gender = get('gender') || 'm';
    $scope.weightWant = get('weightWant');
    $scope.options = [];
    $scope.optionsMap = {};

    $scope.$watch('weightWant', function(val) {
        set('weightWant', val);
    });

    $scope.$watchGroup(['proteinsK', 'weightWant'], function(val) {
        $scope.proteinsDay = (val[0] || 0) * (val[1] || 0);
    });
    $scope.$watchGroup(['fatsK', 'weightFact'], function(val) {
        $scope.fatsDay = (val[0] || 0) * (val[1] || 0);
    });
    $scope.$watchGroup(['carbohydratesK', 'weightWant'], function(val) {
        $scope.carbohydratesDay = (val[0] || 0) * (val[1] || 0);
    });

    $scope.$watchGroup(['proteinsBreakfast', 'proteinsDinner', 'proteinsEvening', 'fatsBreakfast', 'fatsDinner', 'fatsEvening', 'carbohydratesBreakfast', 'carbohydratesDinner', 'carbohydratesEvening', 'proteinsDay', 'fatsDay', 'carbohydratesDay'], function(val) {
        $scope.pb = val[0]*val[9]/100;
        $scope.pd = val[1]*val[9]/100;
        $scope.pe = val[2]*val[9]/100;
        $scope.fb = val[3]*val[10]/100;
        $scope.fd = val[4]*val[10]/100;
        $scope.fe = val[5]*val[10]/100;
        $scope.cb = val[6]*val[11]/100;
        $scope.cd = val[7]*val[11]/100;
        $scope.ce = val[8]*val[11]/100;
    });

    $scope.r = function (x, p) {
        var e = Math.pow(10, p || 0);
        return Math.round(x * e) / e || 0;
    };

    $scope.getCals = function (p, f, c) {
        return $scope.r(p * 4 + f * 9 + c * 4);
    };

    $scope.$watch('gender', function (val) {
        set('gender', val);

        var i = val === 'f' ? 1 : 2;

        $scope.optionsMap = {};

        $scope.options = [['свой набор', [], []]].concat(types).map(function(item) {
            $scope.optionsMap[item[0]] = item[i];

            return item[0];
        });

        $scope.preset = $scope.preset !== 'свой набор' && $scope.preset || $scope.options[0];
        $scope.presetValue = $scope.optionsMap[$scope.preset];
    });

    $scope.$watch('preset', function (val) {
        $scope.presetValue = $scope.optionsMap[val];
    });

    $scope.$watch('presetValue', function (val) {
        var v = val;

        if (!v) return;

        $scope.proteinsK = v[0];
        $scope.fatsK = v[1];
        $scope.carbohydratesK = v[2];

        $scope.proteinsBreakfast = v[3];
        $scope.proteinsDinner = v[4];
        $scope.proteinsEvening = v[5];
        $scope.fatsBreakfast = v[6];
        $scope.fatsDinner = v[7];
        $scope.fatsEvening = v[8];
        $scope.carbohydratesBreakfast = v[9];
        $scope.carbohydratesDinner = v[10];
        $scope.carbohydratesEvening = v[11];
    });
});

function set(key, value) {
    window.localStorage && window.localStorage.setItem(key, JSON.stringify(value));
}

function get(key) {
    return window.localStorage && JSON.parse(window.localStorage.getItem(key));
}
