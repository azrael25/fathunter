var express = require('express'),
    geoip = require('geoip-lite'),
    moment = require('moment'),
    auth = require('http-auth'),
    basic = auth.basic({
        file: __dirname + '/admins'
    }),
    fs = require('fs'),
	app = express(),
	port = 80,
    db = 'users.txt',
    k = 0;

fs.appendFile(db, '\n--- Server reload ---\n', function (err) {});

app.get('/', function (req, res, next) {
    var ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress,
        geo = geoip.lookup(ip),
        ua = req.headers['user-agent'],
        data = '';

    data += (k++) + ' ---------------------------------\n';
    data += 'Date: ' + moment().utcOffset(0).format('DD.MM.YYYY HH:mm:ss') + ' UTC+0\n';
    data += 'IP: ' + ip + '\n';
    data += 'Location: ' + (geo && [geo.country, geo.region, geo.city].join(', ') || 'N/A') + '\n';
    data += 'UserAgent: ' + ua + '\n\n';

    fs.appendFile(db, data, function (err) {});
    next();
});

app.get('/admin', auth.connect(basic), function(req, res) {
    res.end(fs.readFileSync(db));
});

app.use(express.static('./public'));

app.listen(port, function() {
	console.log('Listening on port', port);
});
